package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	progressbar "github.com/schollz/progressbar/v3"
)

const (
	defaultReadTimeout       = time.Second
	defaultReadHeaderTimeout = time.Second
	defaultWriteTimeout      = time.Hour * 24
	defaultIdleTimeout       = time.Hour
	defaultPort              = 8080
)

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("Usage:\n\t%s <FILENAME> <PORT>\n\n", os.Args[0])
		os.Exit(1)
	}
	ctx := context.Background()
	sl := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	if err := run(ctx, sl); err != nil {
		sl.Error("run", slog.Any("err", err))
		os.Exit(1)
	}
}

func run(ctx context.Context, sl *slog.Logger) error {
	handleErr := func(err error) error {
		return fmt.Errorf("run: %w", err)
	}
	ctx, cancelCtx := signal.NotifyContext(ctx, os.Interrupt)
	defer cancelCtx()
	file := os.Args[1]
	f, err := os.Open(file)
	if err != nil {
		return handleErr(err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			sl.Error("close file", slog.Any("err", err))
		}
	}()
	portStr := os.Args[2]
	port, err := strconv.Atoi(portStr)
	if err != nil {
		port = defaultPort
	}
	mux := http.ServeMux{}
	srv := http.Server{
		Addr:              fmt.Sprintf(":%d", port),
		ReadTimeout:       defaultReadTimeout,
		WriteTimeout:      defaultWriteTimeout,
		ReadHeaderTimeout: defaultReadHeaderTimeout,
		IdleTimeout:       defaultIdleTimeout,
		Handler:           &mux,
	}
	fInfo, err := f.Stat()
	if err != nil {
		return handleErr(err)
	}
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Body != nil {
			if err := r.Body.Close(); err != nil {
				sl.Error("close req body", slog.Any("err", err))
			}
		}
		sl.Debug("connection attempt", slog.Any("remote addr", r.RemoteAddr), slog.Any("uri", r.RequestURI))
		w.WriteHeader(http.StatusNotFound)
	})
	mux.HandleFunc(fmt.Sprintf("/%s", fInfo.Name()), func(w http.ResponseWriter, r *http.Request) {
		if r.Body != nil {
			if err := r.Body.Close(); err != nil {
				sl.Error("close req body", slog.Any("err", err))
			}
		}
		sz := strconv.FormatInt(fInfo.Size(), 10)
		progressBar := progressbar.DefaultBytes(fInfo.Size(), fmt.Sprintf("serving %s to %s", sz, r.RemoteAddr))
		w.Header().Set("content-length", sz)
		tr := io.TeeReader(f, progressBar)
		if _, err := io.Copy(w, tr); err != nil {
			sl.Error("copy failed", slog.Any("err", err))
		}
		if _, err := f.Seek(0, 0); err != nil {
			sl.Error("seek failed", slog.Any("err", err))
		}
	})
	go func() {
		<-ctx.Done()
		shutdownCtx, cancelShutdownCtx := context.WithTimeout(context.Background(), 5*time.Second)
		srv.Shutdown(shutdownCtx)
		cancelShutdownCtx()
	}()
	sl.Debug("listening", slog.Any("port", port))
	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		return handleErr(err)
	}
	return nil
}
